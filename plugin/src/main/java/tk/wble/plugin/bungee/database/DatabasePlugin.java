package tk.wble.plugin.bungee.database;

import lombok.extern.java.Log;
import net.md_5.bungee.api.plugin.Plugin;

@Log
public class DatabasePlugin extends Plugin {

    @Override
    public void onEnable() {
        log.info("Enabled DatabaseAPI.");
    }

    @Override
    public void onDisable() {
        log.info("Disabled DatabaseAPI.");
    }

    @Override
    public void onLoad() {
        log.info("Loaded DatabaseAPI.");
    }


}
