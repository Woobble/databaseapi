package tk.wble.plugin.spigot.database;

import lombok.extern.java.Log;
import org.bukkit.plugin.java.JavaPlugin;

@Log
public class DatabasePlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        log.info("Enabled DatabaseAPI.");
    }

    @Override
    public void onDisable() {
        log.info("Disabled DatabaseAPI.");
    }

    @Override
    public void onLoad() {
        log.info("Loaded DatabaseAPI.");
    }

}
