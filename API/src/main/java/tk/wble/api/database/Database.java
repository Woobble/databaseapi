package tk.wble.api.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Future;
import java.util.function.Function;

public interface Database {

    /**
     * Execute SQL update.
     * @param qry update string
     * @param objects qry parameter
     * @return return value of PreparedStatement update execution
     */
    int executeUpdate(String qry, Object... objects);

    /**
     * Execute SQL update async. But notice if you get the Future, it will block the Thread.
     * @param qry update string
     * @param objects qry parameter
     * @return value of PreparedStatement update execution in Future
     */
    Future<Integer> executeUpdateAsny(String qry, Object... objects);

    /**
     * Execute SQL Query.
     * @param qry update string
     * @param function applies the ResultSet
     * @param objects qry parameter
     * @param <T> return type
     * @return you applied object in function
     */
    <T> T executeQuery(String qry, Function<ResultSet, T> function, Object... objects);

    /**
     * Execute SQL Query. But notice if you get the Future, it will block the Thread.
     * @param qry update string
     * @param function applies the ResultSet
     * @param objects qry parameter
     * @param <T> return type
     * @return you applied object in function in Future
     */
    <T> Future<T> executeQueryAsync(String qry, Function<ResultSet, T> function, Object... objects);

    /**
     * Get a SQL connection.
     * @return new SQL connection.
     * @throws SQLException at SQL error
     */
    Connection getConnection() throws SQLException;

}
