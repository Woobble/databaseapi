package tk.wble.api.database;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Executor {

    public static final ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(EXECUTOR_SERVICE::shutdown));
    }

}