package tk.wble.api.database;

import lombok.SneakyThrows;

import java.io.File;

public final class H2Database extends DatabaseImpl {

    protected H2Database(String name) {
        this.source.setDriverClassName("org.h2.Driver");
        this.source.setJdbcUrl("jdbc:h2:mem:" + name);
    }

    @SneakyThrows
    protected H2Database(File file, String name) {
        this(name);
        this.source.setJdbcUrl("jdbc:h2:file:" + file.getAbsolutePath() + "\\" + name + ";AUTO_SERVER=TRUE;AUTO_RECONNECT=TRUE");
    }

    protected H2Database(File file, String name, String username, String password) {
        this(file, name);
        this.source.setUsername(username);
        this.source.setPassword(password);
    }

}
