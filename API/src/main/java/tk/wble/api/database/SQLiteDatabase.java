package tk.wble.api.database;

import lombok.SneakyThrows;
import org.sqlite.JDBC;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

public final class SQLiteDatabase extends DatabaseImpl{

    private Connection connection;

    protected SQLiteDatabase() {
        super();
        this.source.setDriverClassName("org.sqlite.JDBC");
        this.source.setJdbcUrl(String.format("%s:memory:", JDBC.PREFIX));
    }

    /**
     * Database file is auto created
     * @param file database file
     */
    @SneakyThrows
    protected SQLiteDatabase(File file) {
        this();
        if(!file.exists()) {
            if(!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            file.createNewFile();
        }
        this.source.setJdbcUrl(String.format("%s%s", JDBC.PREFIX, file.getAbsoluteFile()));
    }

    /**
     * Execute SQL update.
     *
     * @param qry     update string
     * @param objects qry parameter
     * @return return value of PreparedStatement update execution
     */
    @Override
    public int executeUpdate(String qry, Object... objects) {
        try(PreparedStatement statement = getConnection().prepareStatement(qry)) {
            int i = 1;
            for (Object object : objects) {
                statement.setObject(i++, object.toString());
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Execute SQL Query.
     *
     * @param qry      update string
     * @param function applies the ResultSet
     * @param objects  qry parameter
     * @return you applied object in function
     */
    @Override
    public <T> T executeQuery(String qry, Function<ResultSet, T> function, Object... objects) {
        try(PreparedStatement statement = getConnection().prepareStatement(qry)) {
            int i = 1;
            for (Object object : objects) {
                statement.setObject(i++, object.toString());
            }
            return function.apply(statement.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get a SQL connection.
     *
     * @return new SQL connection.
     * @throws SQLException at SQL error
     */
    @Override
    public Connection getConnection() throws SQLException {
        if(connection == null) {
            connection = this.source.getConnection();
        }
        return connection;
    }

}
