package tk.wble.api.database;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.java.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.logging.Level;

@Log
class DatabaseImpl implements Database {

    protected HikariDataSource source;

    protected DatabaseImpl() {
        this.source = new HikariDataSource();
    }

    /**
     * Execute SQL update.
     *
     * @param qry     update string
     * @param objects qry parameter
     * @return return value of PreparedStatement update execution
     */
    @Override
    public int executeUpdate(String qry, Object... objects) {
        try(Connection conn = getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement(qry);
            int i = 1;
            for (Object object : objects) {
                preparedStatement.setObject(i++, object.toString());
            }
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        return -1;
    }

    /**
     * Execute SQL update async. But notice if you get the Future, it will block the Thread.
     *
     * @param qry     update string
     * @param objects qry parameter
     * @return value of PreparedStatement update execution in Future
     */
    @Override
    public Future<Integer> executeUpdateAsny(String qry, Object... objects) {
        return Executor.EXECUTOR_SERVICE.submit(() -> executeUpdate(qry, objects));
    }

    /**
     * Execute SQL Query.
     *
     * @param qry      update string
     * @param function applies the ResultSet
     * @param objects  qry parameter
     * @return you applied object in function
     */
    @Override
    public <T> T executeQuery(String qry, Function<ResultSet, T> function, Object... objects) {
        try(Connection conn = getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement(qry);
            int i = 1;
            for (Object object : objects) {
                preparedStatement.setObject(i++, object.toString());
            }
            return function.apply(preparedStatement.executeQuery());
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Execute SQL Query. But notice if you get the Future, it will block the Thread.
     *
     * @param qry      update string
     * @param function applies the ResultSet
     * @param objects  qry parameter
     * @return you applied object in function in Future
     */
    @Override
    public <T> Future<T> executeQueryAsync(String qry, Function<ResultSet, T> function, Object... objects) {
        return Executor.EXECUTOR_SERVICE.submit(() -> executeQuery(qry, function, objects));
    }

    /**
     * Get a SQL connection.
     *
     * @return new SQL connection.
     * @throws SQLException at SQL error
     */
    @Override
    public Connection getConnection() throws SQLException {
        return this.source.getConnection();
    }

}