package tk.wble.api.database;

import java.io.File;

public final class DatabaseFactory {

    private DatabaseFactory() {
    }

    public static Database createSQLiteDatabase() {
        return new SQLiteDatabase();
    }

    public static Database createSQLiteDatabase(File file) {
        return new SQLiteDatabase(file);
    }

    public static Database createMySQLDatabase(String host, int port, String username, String password, String database) {
        return new MySQLDatabase(host, port, username, password, database);
    }

    public static Database createMySQLDatabase(String host, String username, String password, String database) {
        return new MySQLDatabase(host, 3306, username, password, database);
    }

    public static Database createMySQLDatabase(String host, int port, String username, String database)  {
        return new MySQLDatabase(host, port, username, "", database);
    }

    public static Database createMySQLDatabase(String host, String username, String database) {
        return new MySQLDatabase(host, 3306, username, "", database);
    }

    public static Database createH2Database(String name) {
        return new H2Database(name);
    }

    public static Database createH2Database(File file, String name) {
        return new H2Database(file, name);
    }

    public static Database createH2Database(File file, String name, String username, String password) {
        return new H2Database(file, name, username, password);
    }

    public static Database createPostgreSQLDatabase(String host, int port, String username, String password, String database) {
        return new PostgreSQLDatabase(host, port, username, password, database);
    }

    public static Database createPostgreSQLDatabase(String host, String username, String password, String database) {
        return new PostgreSQLDatabase(host, 3306, username, password, database);
    }

    public static Database createPostgreSQLDatabase(String host, int port, String username, String database)  {
        return new PostgreSQLDatabase(host, port, username, "", database);
    }

    public static Database createPostgreSQLDatabase(String host, String username, String database) {
        return new PostgreSQLDatabase(host, 3306, username, "", database);
    }

}