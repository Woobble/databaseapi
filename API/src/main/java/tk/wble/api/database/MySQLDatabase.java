package tk.wble.api.database;

public final class MySQLDatabase extends DatabaseImpl {

    protected MySQLDatabase(String host, int port, String username, String password, String database) {
        super();
        this.source.setDriverClassName("com.mysql.jdbc.Driver");
        this.source.setJdbcUrl(String.format("jdbc:mysql://%s:%s/%s", host, port, database));
        this.source.setUsername(username);
        this.source.setPassword(password);
    }

}
